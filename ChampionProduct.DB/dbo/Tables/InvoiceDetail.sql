﻿CREATE TABLE [dbo].[InvoiceDetail] (
    [InvoiceDetailId] BIGINT          IDENTITY (1, 1) NOT NULL,
    [InvoiceId]       BIGINT          NOT NULL,
    [ProductId]       INT             NOT NULL,
    [Amount]          DECIMAL (18, 2) CONSTRAINT [DF_InvoiceDetail_Amount] DEFAULT ((0.00)) NOT NULL,
    [CreatedDate]     DATETIME        CONSTRAINT [DF_InvoiceDetail_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [DeletedDate]     DATETIME        NULL,
    CONSTRAINT [PK_InvoiceDetail] PRIMARY KEY CLUSTERED ([InvoiceDetailId] ASC),
    CONSTRAINT [FK_InvoiceDetail_Invoice] FOREIGN KEY ([InvoiceId]) REFERENCES [dbo].[Invoice] ([InvoiceId])
);


GO
CREATE NONCLUSTERED INDEX [IX_InvoiceDetail_InvoiceId]
    ON [dbo].[InvoiceDetail]([InvoiceId] ASC);