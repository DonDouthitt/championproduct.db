﻿;WITH cteGetAllDatabases (database_id, [name]) AS
	(
		SELECT 
			d.database_id
			,d.[name]
		FROM sys.databases d
	)
SELECT 
	database_id
	,[name]
FROM cteGetAllDatabases


;WITH cteGetAllTableDefinitions(TableName, ColumnName) AS
	(
		SELECT 
			t.[name] AS TableName
			,c.[name] AS ColumnName
		FROM sysobjects AS t
			INNER JOIN syscolumns AS c
				ON (t.id = c.id)
		WHERE t.xtype = 'U'
	)
SELECT 
	TableName
	,ColumnName
FROM cteGetAllTableDefinitions